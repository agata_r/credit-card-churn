# Description

Classification task - prediction, which customer is likely to get churned and resign from credit card services

# Content

- Exploratory Data Analysis
- Feature selection using SelectKBest, chi2
- Scaling data
- Prediction with LGBM Classifier 
- Hyperparameter tuning

